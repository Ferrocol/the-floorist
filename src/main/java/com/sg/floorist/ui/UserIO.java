package com.sg.floorist.ui;

public interface UserIO {

    Double printPromptAndReadDouble(String prompt);

    Double printPromptAndReadDouble(String prompt, double min, double max);

    Float printPromptAndReadFloat(String prompt);

    Float printPromptAndReadFloat(String prompt, float min, float max);

    Integer printPromptAndReadInt(String prompt);

    Integer printPromptAndReadInt(String prompt, int min, int max);

    Long printPromptAndReadLong(String prompt);

    Long printPromptAndReadLong(String prompt, long min, long max);

    String printPromptAndReadString(String prompt);

    boolean printPromptAndAskYesNo(String prompt);
}
