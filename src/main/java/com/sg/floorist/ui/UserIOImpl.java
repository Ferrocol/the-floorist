package com.sg.floorist.ui;

import java.util.Scanner;

public class UserIOImpl implements UserIO {
    private Scanner scanner = new Scanner(System.in);

    @Override
    public Double printPromptAndReadDouble(String prompt) {
        System.out.print(prompt);
        return Double.parseDouble(scanner.nextLine());
    }

    @Override
    public Double printPromptAndReadDouble(String prompt, double min, double max) {
        double d;
        do {
            System.out.print(prompt);
            d = Double.parseDouble(scanner.nextLine());
            if (d < min || d > max) {
                System.out.println("Invalid input. Enter a number from " + min + " to " + max);
            } else {
                return d;
            }
        } while (true);
    }

    @Override
    public Float printPromptAndReadFloat(String prompt) {
        System.out.print(prompt);
        return Float.parseFloat(scanner.nextLine());
    }

    @Override
    public Float printPromptAndReadFloat(String prompt, float min, float max) {
        float f;
        do {
            System.out.print(prompt);
            f = Float.parseFloat(scanner.nextLine());
            if (f < min || f > max) {
                System.out.println("Invalid input. Enter a number from " + min + " to " + max);
            } else {
                return f;
            }
        } while (true);
    }

    @Override
    public Integer printPromptAndReadInt(String prompt) {
        System.out.print(prompt);
        return Integer.parseInt(scanner.nextLine());
    }

    @Override
    public Integer printPromptAndReadInt(String prompt, int min, int max) {
        int i;
        do {
            System.out.print(prompt);
            i = Integer.parseInt(scanner.nextLine());
            if (i < min || i > max) {
                System.out.println("Invalid input. Enter a number from " + min + " to " + max);
            } else {
                return i;
            }
        } while (true);
    }

    @Override
    public Long printPromptAndReadLong(String prompt) {
        System.out.print(prompt);
        return Long.parseLong(scanner.nextLine());
    }

    @Override
    public Long printPromptAndReadLong(String prompt, long min, long max) {
        long l;
        do {
            System.out.print(prompt);
            l = Long.parseLong(scanner.nextLine());
            if (l < min || l > max) {
                System.out.println("Invalid input. Enter a number from " + min + " to " + max);
            } else {
                return l;
            }
        } while (true);
    }

    @Override
    public String printPromptAndReadString(String prompt) {
        System.out.print(prompt);
        return scanner.nextLine();
    }

    @Override
    public boolean printPromptAndAskYesNo(String prompt) {
        System.out.println(prompt);
        String input;
        do {
            input = printPromptAndReadString("Enter y/n:");
        } while (!input.equalsIgnoreCase("y") && !input.equalsIgnoreCase("n"));
        return (input.equalsIgnoreCase("y"));
    }
}
