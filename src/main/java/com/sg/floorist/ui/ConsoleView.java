package com.sg.floorist.ui;

import com.sg.floorist.model.Order;
import com.sg.floorist.model.Product;
import com.sg.floorist.model.Tax;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

public class ConsoleView {
    private UserIO io;

    public ConsoleView(UserIO io) {
        this.io = io;
    }

    public Integer printMenuAndAskUserForSelection() {
        System.out.println("=== MAIN MENU ===");
        System.out.println("1. Display orders for a particular date");
        System.out.println("2. Create an order");
        System.out.println("3. Update an existing order");
        System.out.println("4. Delete an order");
        System.out.println("5. Save changes");
        System.out.println("0. Exit");

        Integer menuSelection = null;
        while (menuSelection == null) {
            try {
                menuSelection = io.printPromptAndReadInt("Please enter a selection from the menu options: ", 0, 5);
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a numeric selection between 0 and 5.");
            }
        }
        return menuSelection;
    }

    public LocalDate askUserForOrderDate() {
        System.out.println("=== ENTER ORDER DATE ===");
        LocalDate date = null;
        while(date == null) {
            try {
                date = LocalDate.parse(io.printPromptAndReadString("Enter order date (MM/dd/yyyy): "),
                        DateTimeFormatter.ofPattern("MM/dd/yyyy"));
            } catch (DateTimeParseException e) {
                System.out.println("Invalid date. Please enter two digits for the month, "
                        + "two digits for the day, and four digits for the year (eg., 02/29/2016).");
            }
        }
        return date;
    }

    public void printOrders(List<Order> orders) {
        System.out.println("=== DISPLAY ORDERS FOR GIVEN DATE ===");
        orders.forEach(o -> {
            System.out.println("Order number: " + o.getOrderNumber());
            printOrderInformation(o);
        });
    }

    private void printOrderInformation(Order order) {
        System.out.println("Customer name: " + order.getCustomerName());
        System.out.println("State: " + order.getState());
        System.out.println("Tax rate: " + order.getTaxRate() + "%");
        System.out.println("Product type: " + order.getProductType());
        System.out.println("Area: " + order.getArea() + " sq. ft.");
        System.out.println("Cost per sq. ft.: $" + order.getMaterialCostPerSquareFoot());
        System.out.println("Labor cost per sq. ft.: $" + order.getLaborCostPerSquareFoot());
        System.out.println("Material cost: $" + order.getMaterialCost());
        System.out.println("Labor cost: $" + order.getLaborCost());
        System.out.println("Tax: $" + order.getTax());
        System.out.println("Total: $" + order.getTotal() + "\n");
    }

    public Order askUserForNewOrderInfo(List<Tax> taxes, List<Product> products) {
        System.out.println("=== CREATE NEW ORDER ===");
        String customerName = io.printPromptAndReadString("Customer Name: ");
        printAllStateTaxRates(taxes);
        String state = io.printPromptAndReadString("Enter state selection as it is displayed above: ");
        printAllProducts(products);
        String productType = io.printPromptAndReadString("Enter product selection as it is displayed above: ");
        BigDecimal area = null;
        while (area == null) {
            try {
                area = new BigDecimal(io.printPromptAndReadString("Area (sq. ft.): ").trim());
            } catch (NumberFormatException e) {
                System.out.println("A numeric (integer or decimal) value for area is required.");
            }
        }
        return new Order(customerName, state, productType, area);
    }

    private void printAllStateTaxRates(List<Tax> taxes) {
        taxes.forEach(t -> System.out.println("State: " + t.getState() + "\tTax Rate: " + t.getTaxRate()));
    }

    private void printAllProducts(List<Product> products) {
        System.out.println("Product:");
        products.forEach(p -> {
            System.out.println(p.getType());
            System.out.println("\t\tMaterial cost per sq. ft.: $" + p.getMaterialCostPerSquareFoot());
            System.out.println("\t\tLabor cost per sq. ft.: $" + p.getLaborCostPerSquareFoot());
                });
    }

    public boolean askUserToConfirmCreate(Order newOrder) {
        System.out.println("=== REVIEW NEW ORDER ===");
        printOrderInformation(newOrder);
        return io.printPromptAndAskYesNo("\nWould you like to add this order?");
    }

    public void printAddCancelMessage() {
        System.out.println("Order creation has been cancelled.");
    }

    public void printAddSuccessMessage() {
        System.out.println("Order successfully created.");
    }

    public Integer askUserForOrderNumber() {
        System.out.println("=== ENTER ORDER NUMBER ===");
        Integer orderNumber = null;
        while (orderNumber == null) {
            try {
                orderNumber = io.printPromptAndReadInt("Please enter the number of the order to be retrieved: ");
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter only numeric digits for the order number.");
            }
        }
        return orderNumber;
    }

    public Order askUserForUpdatedOrderInfo(List<Tax> taxes, List<Product> products, Order order) {
        System.out.println("=== EDIT ORDER " + order.getOrderNumber() + " ===");
        String customerName = io.printPromptAndReadString("Customer name: " + order.getCustomerName()
                + "\nUpdate name or hit return to skip: ");
        if (customerName.trim().length() > 0) {
            order.setCustomerName(customerName);
        }
        printAllStateTaxRates(taxes);
        String state = io.printPromptAndReadString("\nSelected state: " + order.getState()
                + "\nUpdate state or hit return to skip: ");
        if (state.trim().length() > 0) {
            order.setState(state);
        }
        printAllProducts(products);
        String productType = io.printPromptAndReadString("\nSelected product: " + order.getProductType()
                + "\nUpdate product type or hit return to skip: ");
        if (productType.trim().length() > 0) {
            order.setProductType(productType);
        }
        BigDecimal area = null;
        while (area == null) {
            String areaStr = io.printPromptAndReadString("Area (sq. ft): " + order.getArea()
                    + "\nUpdate square footage or hit return to skip: ");
            if (areaStr.trim().length() > 0) {
                try {
                    order.setArea(new BigDecimal(areaStr));
                } catch (NumberFormatException e) {
                    System.out.println("A numeric (integer or decimal) value for area is required.");
                }
            } else {
                area = order.getArea();
            }
        }
        return order;
    }

    public void printUpdateSuccessMessage(Order updatedOrder) {
        System.out.println("Order " + updatedOrder.getOrderNumber() + " successfully updated:");
        printOrderInformation(updatedOrder);
    }

    public boolean askUserToConfirmDelete(Order order) {
        System.out.println("=== DELETE ORDER " + order.getOrderNumber() + " ===");
        printOrderInformation(order);
        return io.printPromptAndAskYesNo("\nWould you like to delete this order?");
    }

    public void printDeleteCancelMessage() {
        System.out.println("Order deletion has been cancelled.");
    }

    public void printDeleteSuccessMessage() {
        System.out.println("Order successfully deleted.");
    }

    public void printSaveSuccessMessage() {
        System.out.println("Changes successfully saved.");
    }

    public boolean askUserIfShouldSave() {
        return io.printPromptAndAskYesNo("Unsaved changes from this session may be lost. Save?");
    }

    public void promptUserEnterToContinue() {
        io.printPromptAndReadString("Press enter to continue.");
    }

    public void printExitMessage() {
        System.out.println("Goodbye!");
    }

    public void printErrorMessage(String errorMessage) {
        System.out.println("=== ERROR ===");
        System.out.println(errorMessage);
    }
}
