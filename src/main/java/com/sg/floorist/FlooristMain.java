package com.sg.floorist;

import com.sg.floorist.controller.FlooristController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class FlooristMain {

    public static void main(String[] args) {
        final String CONFIGURATION_FILE = "config.txt";

        String config;
        try {
            Scanner scanner = new Scanner(new BufferedReader(new FileReader(CONFIGURATION_FILE)));
            config = scanner.nextLine().toLowerCase();
            scanner.close();
        } catch (FileNotFoundException e) {
            config = "train";
            System.out.println("Configuration file not found. Application will run in training mode.");
        }

        ApplicationContext context = new ClassPathXmlApplicationContext(config + "ApplicationContext.xml");
        FlooristController controller = context.getBean("controller", FlooristController.class);
        controller.runOrderManager();
    }
}
