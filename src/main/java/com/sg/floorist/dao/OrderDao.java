package com.sg.floorist.dao;

import com.sg.floorist.exception.DataPersistenceException;
import com.sg.floorist.model.Order;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface OrderDao {
    Map<LocalDate, List<Order>> getOrders(LocalDate date) throws DataPersistenceException;
    Order add(Order newOrder);
    Order update(Order updatedOrder);
    Order remove(Order order);
    void saveChanges() throws DataPersistenceException;
}
