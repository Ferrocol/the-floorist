package com.sg.floorist.dao;

import com.sg.floorist.exception.DataPersistenceException;
import com.sg.floorist.model.Tax;

import java.util.List;

public interface TaxDao {
    List<Tax> getAllStateTaxRates() throws DataPersistenceException;
}
