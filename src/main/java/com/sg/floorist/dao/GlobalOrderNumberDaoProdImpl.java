package com.sg.floorist.dao;

import com.sg.floorist.exception.DataPersistenceException;

import java.io.*;
import java.util.Scanner;

public class GlobalOrderNumberDaoProdImpl implements GlobalOrderNumberDao {
    private static final String GLOBAL_COUNT_FILE = "orders/lastOrderNumber.txt";

    @Override
    public Integer getNextOrderNumber() throws DataPersistenceException {
        Integer currentOrderNumber = loadLastOrderNumber() + 1;
        saveCurrentOrderNumber(currentOrderNumber);
        return currentOrderNumber;
    }

    private Integer loadLastOrderNumber() throws DataPersistenceException {
        Scanner scanner;
        try {
            scanner = new Scanner(new BufferedReader(new FileReader(GLOBAL_COUNT_FILE)));
        } catch (FileNotFoundException e) {
            throw new DataPersistenceException("Could not retrieve order number.", e);
        }
        Integer lastOrderNumber = Integer.parseInt(scanner.nextLine());
        scanner.close();
        return lastOrderNumber;
    }

    private void saveCurrentOrderNumber(Integer currentOrderNumber) throws DataPersistenceException {
        PrintWriter out;
        try {
            out = new PrintWriter(new FileWriter(GLOBAL_COUNT_FILE));
        } catch (IOException e) {
            throw new DataPersistenceException("Could not save next sequential order number.", e);
        }
        out.println(currentOrderNumber);
        out.flush();
        out.close();
    }
}
