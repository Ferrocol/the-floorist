package com.sg.floorist.dao;

import com.sg.floorist.exception.DataPersistenceException;
import com.sg.floorist.model.GlobalOrderNumber;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class GlobalOrderNumberDaoTrainImpl extends GlobalOrderNumberDaoProdImpl{
    private static final String GLOBAL_COUNT_FILE = "orders/lastOrderNumber.txt";

    private GlobalOrderNumber orderNumber = new GlobalOrderNumber();

    @Override
    public Integer getNextOrderNumber() throws DataPersistenceException {
        if (orderNumber.getValue() == null) {
            orderNumber.setValue(loadLastOrderNumber() + 1);
        } else {
            orderNumber.setValue(orderNumber.getValue() + 1);
        }
        return orderNumber.getValue();
    }

    private Integer loadLastOrderNumber() throws DataPersistenceException {
        Scanner scanner;
        try {
            scanner = new Scanner(new BufferedReader(new FileReader(GLOBAL_COUNT_FILE)));
        } catch (FileNotFoundException e) {
            throw new DataPersistenceException("Could not retrieve order number.", e);
        }
        Integer lastOrderNumber = Integer.parseInt(scanner.nextLine());
        scanner.close();
        return lastOrderNumber;
    }
}
