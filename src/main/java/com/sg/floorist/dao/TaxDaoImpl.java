package com.sg.floorist.dao;

import com.sg.floorist.exception.DataPersistenceException;
import com.sg.floorist.model.Tax;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TaxDaoImpl implements TaxDao {
    private static final String TAXES_FILE = "Taxes.txt";
    private static final String DELIMITER = ",";

    private List<Tax> taxes = new ArrayList<>();

    @Override
    public List<Tax> getAllStateTaxRates() throws DataPersistenceException {
        loadTaxesToList();
        return taxes;
    }

    private void loadTaxesToList() throws DataPersistenceException {
        Scanner scanner;
        try {
            scanner = new Scanner(new BufferedReader(new FileReader(TAXES_FILE)));
        } catch (FileNotFoundException e) {
            throw new DataPersistenceException("Could not load tax information.", e);
        }
        scanner.nextLine();
        while (scanner.hasNextLine()) {
            String currentLine = scanner.nextLine();
            String[] productAttributes = currentLine.split(DELIMITER);
            Tax tax= new Tax(productAttributes[0], productAttributes[1]);
            taxes.add(tax);
        }
        scanner.close();
    }
}
