package com.sg.floorist.dao;

import com.sg.floorist.exception.DataPersistenceException;

public interface AuditDao {
    void writeAuditEntry(String entry) throws DataPersistenceException;
}
