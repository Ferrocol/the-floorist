package com.sg.floorist.dao;

import com.sg.floorist.exception.DataPersistenceException;

public interface GlobalOrderNumberDao {
    Integer getNextOrderNumber() throws DataPersistenceException;
}
