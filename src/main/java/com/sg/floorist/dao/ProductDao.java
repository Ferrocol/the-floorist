package com.sg.floorist.dao;

import com.sg.floorist.exception.DataPersistenceException;
import com.sg.floorist.model.Product;

import java.util.List;

public interface ProductDao {
    List<Product> getAllProducts() throws DataPersistenceException;
}
