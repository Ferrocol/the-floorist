package com.sg.floorist.controller;

import com.sg.floorist.exception.DataPersistenceException;
import com.sg.floorist.model.Order;
import com.sg.floorist.model.OrderValidationResponse;
import com.sg.floorist.model.Product;
import com.sg.floorist.model.Tax;
import com.sg.floorist.service.OrderManagementService;
import com.sg.floorist.ui.ConsoleView;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class FlooristController {
    private OrderManagementService service;
    private ConsoleView view;

    private boolean isSaveRequired = false;
    private List<Tax> taxes = new ArrayList<>();
    private List<Product> products = new ArrayList<>();

    public FlooristController(OrderManagementService service, ConsoleView view) {
        this.service = service;
        this.view = view;
    }

    public void runOrderManager() {
        boolean run = true;
        try {
            taxes = service.getAllStateTaxRates();
            products = service.getAllProducts();
        } catch (DataPersistenceException e) {
            view.printErrorMessage(e.getMessage());
        }
        while (run) {
            Integer menuSelection = view.printMenuAndAskUserForSelection();
            switch (menuSelection) {
                case 1:
                    displayOrdersForDate();
                    break;
                case 2:
                    addOrder();
                    break;
                case 3:
                    updateOrder();
                    break;
                case 4:
                    removeOrder();
                    break;
                case 5:
                    saveChanges();
                    break;
                case 0:
                    if (isSaveRequired && view.askUserIfShouldSave()) {
                        saveChanges();
                    }
                    view.printExitMessage();
                    run = false;
                    break;
            }
        }
    }

    private void displayOrdersForDate() {
        LocalDate date = view.askUserForOrderDate();
        try {
            view.printOrders(service.getOrdersForGivenDate(date));
        } catch (DataPersistenceException e) {
            view.printErrorMessage(e.getMessage());
        }
        view.promptUserEnterToContinue();
    }

    private void addOrder() {
        Order newOrder = view.askUserForNewOrderInfo(taxes, products);
        OrderValidationResponse response = service.create(newOrder);
        if (response.isSuccess() && view.askUserToConfirmCreate(newOrder)) {
            try {
                service.add(newOrder);
                view.printAddSuccessMessage();
                isSaveRequired = true;
            } catch (DataPersistenceException e) {
                view.printErrorMessage(e.getMessage());
            }
        } else if (!response.isSuccess()) {
            view.printErrorMessage(response.getMessage());
        } else {
            view.printAddCancelMessage();
        }
        view.promptUserEnterToContinue();
    }

    private void updateOrder() {
        LocalDate date = view.askUserForOrderDate();
        Integer orderNumber = view.askUserForOrderNumber();
        try {
            service.getOrdersForGivenDate(date);
        } catch (DataPersistenceException e) {
            view.printErrorMessage(e.getMessage());
            return;
        }
        OrderValidationResponse response = service.getOrderByOrderNumber(orderNumber);
        if (response.isSuccess()) {
            Order order = view.askUserForUpdatedOrderInfo(taxes, products, response.getOrder());
            response = service.update(order);
            if (response.isSuccess()) {
                view.printUpdateSuccessMessage(response.getOrder());
                isSaveRequired = true;
            } else {
                view.printErrorMessage(response.getMessage());
            }
        } else {
            view.printErrorMessage(response.getMessage());
        }
        view.promptUserEnterToContinue();
    }

    private void removeOrder() {
        LocalDate date = view.askUserForOrderDate();
        Integer orderNumber = view.askUserForOrderNumber();
        try {
            service.getOrdersForGivenDate(date);
        } catch (DataPersistenceException e) {
            view.printErrorMessage(e.getMessage());
        }
        OrderValidationResponse response = service.getOrderByOrderNumber(orderNumber);
        if (response.isSuccess() && view.askUserToConfirmDelete(response.getOrder())) {
            service.remove(response.getOrder());
            view.printDeleteSuccessMessage();
            isSaveRequired = true;
        } else if (!response.isSuccess()) {
            view.printErrorMessage(response.getMessage());
        } else {
            view.printDeleteCancelMessage();
        }
        view.promptUserEnterToContinue();
    }

    private void saveChanges() {
        try {
            service.saveChanges();
        } catch (DataPersistenceException e) {
            view.printErrorMessage(e.getMessage());
            return;
        }
        view.printSaveSuccessMessage();
        isSaveRequired = false;
        view.promptUserEnterToContinue();
    }
}
