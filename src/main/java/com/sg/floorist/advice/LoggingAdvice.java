package com.sg.floorist.advice;

import com.sg.floorist.dao.AuditDao;
import com.sg.floorist.exception.DataPersistenceException;
import org.aspectj.lang.JoinPoint;

public class LoggingAdvice {
    private AuditDao auditDao;

    public LoggingAdvice(AuditDao auditDao) {
        this.auditDao = auditDao;
    }

    public void createAuditEntry(JoinPoint jp) {
        String auditEntry = jp.getSignature().getName();
        try {
            auditDao.writeAuditEntry(auditEntry);
        } catch (DataPersistenceException dpe) {
            System.err.println("ERROR: Could not create audit entry in LoggingAdvice.");
        }
    }

    public void createAuditEntry(JoinPoint jp, Object returnValue) {
        String auditEntry = jp.getSignature().getName();
        auditEntry += returnValue;
        try {
            auditDao.writeAuditEntry(auditEntry);
        } catch (DataPersistenceException dpe) {
            System.err.println("ERROR: Could not create audit entry in LoggingAdvice.");
        }
    }

    public void createExceptionAuditEntry(JoinPoint jp, Exception e) {
        String auditEntry = jp.getSignature().getName()
                + " |Exception thrown: " + e.getMessage();
        try {
            auditDao.writeAuditEntry(auditEntry);
        } catch (DataPersistenceException dpe) {
            System.err.println("ERROR: Could not create audit entry in LoggingAdvice.");
        }
    }
}
