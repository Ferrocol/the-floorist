package com.sg.floorist.service;

import com.sg.floorist.dao.GlobalOrderNumberDao;
import com.sg.floorist.dao.OrderDao;
import com.sg.floorist.dao.ProductDao;
import com.sg.floorist.dao.TaxDao;
import com.sg.floorist.exception.DataPersistenceException;
import com.sg.floorist.model.Order;
import com.sg.floorist.model.OrderValidationResponse;
import com.sg.floorist.model.Product;
import com.sg.floorist.model.Tax;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OrderManagementServiceImpl implements OrderManagementService {
    private GlobalOrderNumberDao orderNumberDao;
    private OrderDao orderDao;
    private ProductDao productDao;
    private TaxDao taxDao;

    private List<Tax> taxes = new ArrayList<>();
    private List<Product> products = new ArrayList<>();
    private List<Order> ordersForGivenDate = new ArrayList<>();

    public OrderManagementServiceImpl(GlobalOrderNumberDao orderNumberDao, OrderDao orderDao,
                                      ProductDao productDao, TaxDao taxDao) {
        this.orderNumberDao = orderNumberDao;
        this.orderDao = orderDao;
        this.productDao = productDao;
        this.taxDao = taxDao;
    }

    @Override
    public List<Order> getOrdersForGivenDate(LocalDate date) throws DataPersistenceException {
        ordersForGivenDate = orderDao.getOrders(date)
                .entrySet()
                .stream()
                .filter(entry -> entry.getKey().equals(date))
                .flatMap(value -> value.getValue().stream())
                .collect(Collectors.toList());
        return ordersForGivenDate;
    }

    @Override
    public List<Tax> getAllStateTaxRates() throws DataPersistenceException {
        taxes = taxDao.getAllStateTaxRates();
        return taxes;
    }

    @Override
    public List<Product> getAllProducts() throws DataPersistenceException {
        products = productDao.getAllProducts();
        return products;
    }

    @Override
    public OrderValidationResponse create(Order newOrder) {
        OrderValidationResponse validation = isValid(newOrder);
        if (validation.isSuccess()) {
            taxes.forEach(t -> {
                if (t.getState().equalsIgnoreCase(newOrder.getState())) {
                    newOrder.setTaxRate(t.getTaxRate());
                }
            });
            products.forEach(p -> {
                if (p.getType().equalsIgnoreCase(newOrder.getProductType())) {
                    newOrder.setMaterialCostPerSquareFoot(p.getMaterialCostPerSquareFoot());
                    newOrder.setLaborCostPerSquareFoot(p.getLaborCostPerSquareFoot());
                }
            });
            newOrder.calculateMaterialCost();
            newOrder.calculateLaborCost();
            newOrder.calculateTax();
            newOrder.calculateTotal();
            validation.setOrder(newOrder);
        }
        return validation;
    }

    private OrderValidationResponse isValid(Order order) {
        OrderValidationResponse validation = new OrderValidationResponse();

        if (order.getCustomerName().trim().isEmpty()) {
            validation.setMessage("Customer name is required.");
        } else if (order.getState().trim().isEmpty()) {
            validation.setMessage("State is required.");
        } else if (order.getProductType().trim().isEmpty()) {
            validation.setMessage("Product type is required.");
        } else if (order.getArea().compareTo(BigDecimal.ZERO) <= 0) {
            validation.setMessage("Square footage must be positive.");
        } else if (taxes.stream().noneMatch(t -> t.getState().equalsIgnoreCase(order.getState().trim()))) {
            validation.setMessage("The state entered must match one of the available state abbreviations.");
        } else if (products.stream().noneMatch(p -> p.getType().equalsIgnoreCase(order.getProductType().trim()))) {
            validation.setMessage("The selected product must match one of the available product types.");
        } else {
            validation.setSuccess(true);
        }
        return validation;
    }

    @Override
    public Order add(Order newOrder) throws DataPersistenceException {
        newOrder.setOrderNumber(orderNumberDao.getNextOrderNumber());
        return orderDao.add(newOrder);
    }

    @Override
    public OrderValidationResponse getOrderByOrderNumber(Integer orderNumber) {
        return orderExists(orderNumber);
    }

    private OrderValidationResponse orderExists(Integer orderNumber) {
        OrderValidationResponse validation = new OrderValidationResponse();

        for (Order order : ordersForGivenDate) {
            if (order.getOrderNumber().equals(orderNumber)) {
                validation.setOrder(order);
                validation.setSuccess(true);
                return validation;
            } else {
                validation.setMessage("Order number not found.");
            }
        }
        return validation;
    }

    @Override
    public OrderValidationResponse update(Order updatedOrder) {
        OrderValidationResponse validation = isValid(updatedOrder);
        if (validation.isSuccess()) {
            taxes.forEach(t -> {
                if (t.getState().equalsIgnoreCase(updatedOrder.getState().trim())
                        && !t.getTaxRate().equals(updatedOrder.getTaxRate())) {
                    updatedOrder.setTaxRate(t.getTaxRate());
                }
            });
            products.forEach(p -> {
                if (p.getType().equalsIgnoreCase(updatedOrder.getProductType().trim())
                        && !p.getMaterialCostPerSquareFoot().equals(updatedOrder.getMaterialCostPerSquareFoot())
                        && !p.getLaborCostPerSquareFoot().equals(updatedOrder.getLaborCostPerSquareFoot())) {
                    updatedOrder.setMaterialCostPerSquareFoot(p.getMaterialCostPerSquareFoot());
                    updatedOrder.setLaborCostPerSquareFoot(p.getLaborCostPerSquareFoot());
                }
            });
            updatedOrder.calculateMaterialCost();
            updatedOrder.calculateLaborCost();
            updatedOrder.calculateTax();
            updatedOrder.calculateTotal();
            validation.setOrder(orderDao.update(updatedOrder));
        }
        return validation;
    }

    @Override
    public Order remove(Order order) {
        return orderDao.remove(order);
    }

    @Override
    public void saveChanges() throws DataPersistenceException {
        orderDao.saveChanges();
    }
}
