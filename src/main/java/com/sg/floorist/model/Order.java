package com.sg.floorist.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Order {
    private Integer orderNumber;
    private String customerName;
    private String state;
    private BigDecimal taxRate;
    private String productType;
    private BigDecimal area;
    private BigDecimal materialCostPerSquareFoot;
    private BigDecimal laborCostPerSquareFoot;
    private BigDecimal materialCost;
    private BigDecimal laborCost;
    private BigDecimal tax;
    private BigDecimal total;

    public Order(String customerName, String state, String productType, BigDecimal area) {
        this.customerName = customerName;
        this.state = state;
        this.productType = productType;
        this.area = area;
    }

    public Order(String orderNumber, String customerName, String state, String taxRate,
                 String productType, String area, String materialCostPerSquareFoot,
                 String laborCostPerSquareFoot, String materialCost, String laborCost,
                 String tax, String total) {
        this.orderNumber = Integer.parseInt(orderNumber);
        this.customerName = customerName;
        this.state = state;
        this.taxRate = new BigDecimal(taxRate);
        this.productType = productType;
        this.area = new BigDecimal(area);
        this.materialCostPerSquareFoot = new BigDecimal(materialCostPerSquareFoot);
        this.laborCostPerSquareFoot = new BigDecimal(laborCostPerSquareFoot);
        this.materialCost = new BigDecimal(materialCost);
        this.laborCost = new BigDecimal(laborCost);
        this.tax = new BigDecimal(tax);
        this.total = new BigDecimal(total);
    }

    @Override
    public String toString() {
        return " |Order Number: " + orderNumber
                + " |Customer name: " + customerName
                + " |State: " + state
                + " |Tax rate: " + taxRate + "%"
                + " |Product type: " + productType
                + " |Area: " + area + " sq. ft."
                + " |Cost per sq. ft.: $" + materialCostPerSquareFoot
                + " |Labor cost per sq. ft.: $" + laborCostPerSquareFoot
                + " |Material cost: $" + materialCost
                + " |Labor cost: $" + laborCost
                + " |Tax: $" + tax
                + " |Total: $" + total;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public BigDecimal getArea() {
        return area;
    }

    public void setArea(BigDecimal area) {
        this.area = area;
    }

    public BigDecimal getMaterialCostPerSquareFoot() {
        return materialCostPerSquareFoot;
    }

    public void setMaterialCostPerSquareFoot(BigDecimal materialCostPerSquareFoot) {
        this.materialCostPerSquareFoot = materialCostPerSquareFoot;
    }

    public BigDecimal getLaborCostPerSquareFoot() {
        return laborCostPerSquareFoot;
    }

    public void setLaborCostPerSquareFoot(BigDecimal laborCostPerSquareFoot) {
        this.laborCostPerSquareFoot = laborCostPerSquareFoot;
    }

    public BigDecimal getMaterialCost() {
        return materialCost;
    }

    public void calculateMaterialCost() {
        materialCost = materialCostPerSquareFoot.multiply(area).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal getLaborCost() {
        return laborCost;
    }

    public void calculateLaborCost() {
        laborCost = laborCostPerSquareFoot.multiply(area).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void calculateTax() {
        tax = taxRate.multiply(materialCost.add(laborCost))
                .divide(new BigDecimal("100"), 2, RoundingMode.HALF_UP);
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void calculateTotal() {
        total = materialCost.add(laborCost).add(tax);
    }
}
