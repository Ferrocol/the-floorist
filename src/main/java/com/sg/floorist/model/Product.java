package com.sg.floorist.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Product {
    private String type;
    private BigDecimal materialCostPerSquareFoot;
    private BigDecimal laborCostPerSquareFoot;

    public Product(String type, String materialCost, String laborCost) {
        this.type = type;
        this.materialCostPerSquareFoot = new BigDecimal(materialCost).setScale(2, RoundingMode.HALF_UP);
        this.laborCostPerSquareFoot = new BigDecimal(laborCost).setScale(2, RoundingMode.HALF_UP);
    }

    public String getType() {
        return type;
    }

    public BigDecimal getMaterialCostPerSquareFoot() {
        return materialCostPerSquareFoot;
    }

    public BigDecimal getLaborCostPerSquareFoot() {
        return laborCostPerSquareFoot;
    }
}