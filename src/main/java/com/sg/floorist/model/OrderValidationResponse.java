package com.sg.floorist.model;

public class OrderValidationResponse {
    private boolean isSuccess;
    private String message;
    private Order order;

    @Override
    public String toString() {
        if (isSuccess) {
            return order.toString();
        } else {
            return " |Message: " + message;
        }
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
