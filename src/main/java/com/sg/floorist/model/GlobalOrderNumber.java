package com.sg.floorist.model;

public class GlobalOrderNumber {
    private Integer value;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
