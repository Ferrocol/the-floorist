package com.sg.floorist.dao;

import com.sg.floorist.exception.DataPersistenceException;

public class GlobalOrderNumberDaoStubImpl implements GlobalOrderNumberDao {
    @Override
    public Integer getNextOrderNumber() throws DataPersistenceException {
        return -1;
    }
}
