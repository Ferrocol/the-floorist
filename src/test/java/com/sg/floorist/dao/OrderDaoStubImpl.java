package com.sg.floorist.dao;

import com.sg.floorist.exception.DataPersistenceException;
import com.sg.floorist.model.Order;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderDaoStubImpl implements OrderDao {
    @Override
    public Map<LocalDate, List<Order>> getOrders(LocalDate date) throws DataPersistenceException {
        Map<LocalDate, List<Order>> testOrders = new HashMap<>();
        List<Order> minDateOrders = new ArrayList<>();
        minDateOrders.add(new Order("Customer Min", "AK", "Dirt", BigDecimal.TEN));
        testOrders.put(LocalDate.MIN, minDateOrders);
        List<Order> maxDateOrders = new ArrayList<>();
        maxDateOrders.add(new Order("Customer Max", "CA", "Straw", BigDecimal.TEN));
        testOrders.put(LocalDate.MAX, maxDateOrders);
        return testOrders;
    }

    @Override
    public Order add(Order newOrder) {
        return newOrder;
    }

    @Override
    public Order update(Order updatedOrder) {
        return updatedOrder;
    }

    @Override
    public Order remove(Order order) {
        return order;
    }

    @Override
    public void saveChanges() throws DataPersistenceException {

    }
}
