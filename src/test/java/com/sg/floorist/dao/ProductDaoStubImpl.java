package com.sg.floorist.dao;

import com.sg.floorist.exception.DataPersistenceException;
import com.sg.floorist.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductDaoStubImpl implements ProductDao {
    @Override
    public List<Product> getAllProducts() throws DataPersistenceException {
        List<Product> testProducts = new ArrayList<>();
        testProducts.add(new Product("Dirt", "0.10", "0.25"));
        testProducts.add(new Product("Straw", "0.25", "0.50"));
        return testProducts;
    }
}
