package com.sg.floorist.service;

import com.sg.floorist.exception.DataPersistenceException;
import com.sg.floorist.model.Order;
import com.sg.floorist.model.OrderValidationResponse;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

public class OrderManagementServiceTest {
    private OrderManagementService service;

    public OrderManagementServiceTest() {
        ApplicationContext context = new ClassPathXmlApplicationContext("testApplicationContext.xml");
        service = context.getBean("service", OrderManagementService.class);
    }

    @Test
    public void testGetOrdersForMinDateContainsCorrectTestOrder()
            throws DataPersistenceException {
        List<Order> testOrders = service.getOrdersForGivenDate(LocalDate.MIN);
        assertEquals(1, testOrders.size());
        assertEquals("Customer Min", testOrders.get(0).getCustomerName());
        assertEquals("AK", testOrders.get(0).getState());
        assertEquals("Dirt", testOrders.get(0).getProductType());
        assertEquals(BigDecimal.TEN, testOrders.get(0).getArea());
    }

    @Test
    public void testGetOrdersForMaxDateContainsCorrectTestOrder()
            throws DataPersistenceException {
        List<Order> testOrders = service.getOrdersForGivenDate(LocalDate.MAX);
        assertEquals(1, testOrders.size());
        assertEquals("Customer Max", testOrders.get(0).getCustomerName());
        assertEquals("CA", testOrders.get(0).getState());
        assertEquals("Straw", testOrders.get(0).getProductType());
        assertEquals(BigDecimal.TEN, testOrders.get(0).getArea());
    }

    @Test
    public void testOrderWithoutCustomerNameValidationUnsuccessful()
            throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();
        OrderValidationResponse response = service.create(
                new Order("", "AK", "Dirt", BigDecimal.ONE));
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
        assertNull(response.getOrder());
    }

    @Test
    public void testOrderWithSpaceCustomerNameValidationUnsuccessful()
            throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();
        OrderValidationResponse response = service.create(
                new Order(" ", "AK", "Dirt", BigDecimal.ONE));
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
        assertNull(response.getOrder());
    }

    @Test
    public void testOrderWithoutStateValidationUnsuccessful()
            throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();
        OrderValidationResponse response = service.create(
                new Order("Name", "", "Dirt", BigDecimal.ONE));
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
        assertNull(response.getOrder());
    }

    @Test
    public void testOrderWithSpaceStateValidationUnsuccessful()
            throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();
        OrderValidationResponse response = service.create(
                new Order("Name", " ", "Dirt", BigDecimal.ONE));
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
        assertNull(response.getOrder());
    }

    @Test
    public void testOrderWithoutProductTypeValidationUnsuccessful()
            throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();
        OrderValidationResponse response = service.create(
                new Order("Name", "AK", "", BigDecimal.ONE));
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
        assertNull(response.getOrder());
    }

    @Test
    public void testOrderWithSpaceProductTypeValidationUnsuccessful()
            throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();
        OrderValidationResponse response = service.create(new Order(
                "Name", "AK", " ", BigDecimal.ONE));
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
        assertNull(response.getOrder());
    }

    @Test
    public void testOrderWithoutZeroAreaValidationUnsuccessful()
            throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();
        OrderValidationResponse response = service.create(new Order(
                "Name", "AK", "Dirt", BigDecimal.ZERO));
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
        assertNull(response.getOrder());
    }

    @Test
    public void testOrderWithNegativeAreaValidationUnsuccessful()
            throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();
        OrderValidationResponse response = service.create(new Order(
                "Name", "AK", "Dirt", new BigDecimal("-1")));
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
        assertNull(response.getOrder());
    }

    @Test
    public void testOrderWithUnlistedStateValidationUnsuccessful()
            throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();
        OrderValidationResponse response = service.create(new Order(
                        "Name", "MN", "Dirt", BigDecimal.TEN));
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
        assertNull(response.getOrder());
    }

    @Test
    public void testOrderWithUnlistedProductTypeValidationUnsuccessful()
            throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();
        OrderValidationResponse response = service.create(new Order(
                "Name", "AK", "Stone", BigDecimal.TEN));
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
        assertNull(response.getOrder());
    }

    @Test
    public void testValidOrderValidationSuccessful()
            throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();
        OrderValidationResponse response = service.create(
                new Order("Name", "AK", "Dirt", BigDecimal.TEN));
        assertTrue(response.isSuccess());
        assertNull(response.getMessage());
        assertNotNull(response.getOrder());
    }

    @Test
    public void testInvalidOrderValuesNotCalculated()
            throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();
        Order invalidOrder = new Order(" ", " ", " ", BigDecimal.ZERO);
        service.create(invalidOrder);
        assertNull(invalidOrder.getTaxRate());
        assertNull(invalidOrder.getMaterialCostPerSquareFoot());
        assertNull(invalidOrder.getLaborCostPerSquareFoot());
        assertNull(invalidOrder.getMaterialCost());
        assertNull(invalidOrder.getLaborCost());
        assertNull(invalidOrder.getTax());
        assertNull(invalidOrder.getTotal());
    }

    @Test
    public void testValidOrderValuesGetCalculated()
        throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();

        BigDecimal testArea = BigDecimal.TEN;
        Order validOrder = new Order("Name", "AK", "Dirt", testArea);
        validOrder = service.create(validOrder).getOrder();

        BigDecimal expectedTaxRate = new BigDecimal("0.00");
        assertEquals(expectedTaxRate, validOrder.getTaxRate());

        BigDecimal expectedMaterialCostPerSquareFoot = new BigDecimal("0.10");
        assertEquals(expectedMaterialCostPerSquareFoot, validOrder.getMaterialCostPerSquareFoot());

        BigDecimal expectedLaborCostPerSquareFoot = new BigDecimal("0.25");
        assertEquals(expectedLaborCostPerSquareFoot, validOrder.getLaborCostPerSquareFoot());

        BigDecimal expectedMaterialCost = expectedMaterialCostPerSquareFoot.multiply(testArea);
        assertEquals(expectedMaterialCost, validOrder.getMaterialCost());

        BigDecimal expectedLaborCost = expectedLaborCostPerSquareFoot.multiply(testArea);
        assertEquals(expectedLaborCost, validOrder.getLaborCost());

        BigDecimal expectedTax = expectedMaterialCost.add(expectedLaborCost)
                .multiply(expectedTaxRate)
                .setScale(2, RoundingMode.HALF_UP);
        assertEquals(expectedTax, validOrder.getTax());

        BigDecimal expectedTotal = expectedMaterialCost.add(expectedLaborCost).add(expectedTax);
        assertEquals(expectedTotal, validOrder.getTotal());
    }

    @Test
    public void testAddAssignsOrderNumberNegativeOne() throws DataPersistenceException {
        Order validOrder = new Order("Name", "AK", "Dirt", BigDecimal.TEN);
        Integer expectedOrderNumber = -1;
        assertEquals(expectedOrderNumber, service.add(validOrder).getOrderNumber());
    }

    @Test
    public void testOrderByNonExistentOrderNumber() throws DataPersistenceException {
        service.getOrdersForGivenDate(LocalDate.MIN).get(0).setOrderNumber(0);
        OrderValidationResponse response = service.getOrderByOrderNumber(-1);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
        assertNull(response.getOrder());
    }

    @Test
    public void testOrderByOrderNumberNegativeOne() throws DataPersistenceException {
        service.getOrdersForGivenDate(LocalDate.MIN).get(0).setOrderNumber(-1);
        OrderValidationResponse response = service.getOrderByOrderNumber(-1);
        assertTrue(response.isSuccess());
        assertNull(response.getMessage());
        assertNotNull(response.getOrder());
    }

    @Test
    public void testTaxRateUpdatedIfStateChanged()
            throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();
        Order orderWithStateAndTaxRateMismatch = new Order("0", "FirstName LastName", "CA",
                "0.00", "Dirt", "10",
                "0.10", "0.25", "1.00",
                "2.50", "0.00", "3.50");
        OrderValidationResponse response = service.update(orderWithStateAndTaxRateMismatch);
        BigDecimal expectedUpdatedTaxRate = new BigDecimal("7.25");
        assertEquals(expectedUpdatedTaxRate, response.getOrder().getTaxRate());
        BigDecimal expectedUnchangedMaterialCostPerSquareFoot = new BigDecimal("0.10");
        assertEquals(expectedUnchangedMaterialCostPerSquareFoot, response.getOrder().getMaterialCostPerSquareFoot());
        BigDecimal expectedUnchangedLaborCostPerSquareFoot = new BigDecimal("0.25");
        assertEquals(expectedUnchangedLaborCostPerSquareFoot, response.getOrder().getLaborCostPerSquareFoot());
        BigDecimal expectedUpdatedMaterialCost = new BigDecimal("1.00");
        assertEquals(expectedUpdatedMaterialCost, response.getOrder().getMaterialCost());
        BigDecimal expectedUpdatedLaborCost = new BigDecimal("2.50");
        assertEquals(expectedUpdatedLaborCost, response.getOrder().getLaborCost());
        BigDecimal expectedUpdatedTax = new BigDecimal("0.25");
        assertEquals(expectedUpdatedTax, response.getOrder().getTax());
        BigDecimal expectedUpdatedTotal = new BigDecimal("3.75");
        assertEquals(expectedUpdatedTotal, response.getOrder().getTotal());
    }

    @Test
    public void testCostPerSquareFootUpdatedIfProductChanged()
            throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();
        Order orderWithProductAndCostMismatch = new Order("0", "FirstName LastName", "AK",
                "0.00", "Straw", "10",
                "0.10", "0.25", "1.00",
                "2.50", "0.00", "3.50");
        OrderValidationResponse response = service.update(orderWithProductAndCostMismatch);
        BigDecimal expectedUnchangedTaxRate = new BigDecimal("0.00");
        assertEquals(expectedUnchangedTaxRate, response.getOrder().getTaxRate());
        BigDecimal expectedUpdatedMaterialCostPerSquareFoot = new BigDecimal("0.25");
        assertEquals(expectedUpdatedMaterialCostPerSquareFoot, response.getOrder().getMaterialCostPerSquareFoot());
        BigDecimal expectedUpdatedLaborCostPerSquareFoot = new BigDecimal("0.50");
        assertEquals(expectedUpdatedLaborCostPerSquareFoot, response.getOrder().getLaborCostPerSquareFoot());
        BigDecimal expectedUpdatedMaterialCost = new BigDecimal("2.50");
        assertEquals(expectedUpdatedMaterialCost, response.getOrder().getMaterialCost());
        BigDecimal expectedUpdatedLaborCost = new BigDecimal("5.00");
        assertEquals(expectedUpdatedLaborCost, response.getOrder().getLaborCost());
        BigDecimal expectedUpdatedTax = new BigDecimal("0.00");
        assertEquals(expectedUpdatedTax, response.getOrder().getTax());
        BigDecimal expectedUpdatedTotal = new BigDecimal("7.50");
        assertEquals(expectedUpdatedTotal, response.getOrder().getTotal());
    }

    @Test
    public void testValuesRecalculatedWhenAreaChanged()
            throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();
        Order orderWithCorrectStateAndTaxRate = new Order("0", "FirstName LastName", "AK",
                "0.00", "Dirt", "1",
                "0.10", "0.25", "1.00",
                "2.50", "0.00", "3.50");
        OrderValidationResponse response = service.update(orderWithCorrectStateAndTaxRate);
        BigDecimal expectedUnchangedTaxRate = new BigDecimal("0.00");
        assertEquals(expectedUnchangedTaxRate, response.getOrder().getTaxRate());
        BigDecimal expectedUnchangedMaterialCostPerSquareFoot = new BigDecimal("0.10");
        assertEquals(expectedUnchangedMaterialCostPerSquareFoot, response.getOrder().getMaterialCostPerSquareFoot());
        BigDecimal expectedUnchangedLaborCostPerSquareFoot = new BigDecimal("0.25");
        assertEquals(expectedUnchangedLaborCostPerSquareFoot, response.getOrder().getLaborCostPerSquareFoot());
        BigDecimal expectedUpdatedMaterialCost = new BigDecimal("0.10");
        assertEquals(expectedUpdatedMaterialCost, response.getOrder().getMaterialCost());
        BigDecimal expectedUpdatedLaborCost = new BigDecimal("0.25");
        assertEquals(expectedUpdatedLaborCost, response.getOrder().getLaborCost());
        BigDecimal expectedUnchangedTax = new BigDecimal("0.00");
        assertEquals(expectedUnchangedTax, response.getOrder().getTax());
        BigDecimal expectedUpdatedTotal = new BigDecimal("0.35");
        assertEquals(expectedUpdatedTotal, response.getOrder().getTotal());
    }

    @Test
    public void testTaxRateNotUpdatedIfNumericalValuesNotChanged()
            throws DataPersistenceException {
        service.getAllStateTaxRates();
        service.getAllProducts();
        Order orderWithCorrectStateAndTaxRate = new Order("0", "FirstName LastName", "AK",
                "0.00", "Dirt", "10",
                "0.10", "0.25", "1.00",
                "2.50", "0.00", "3.50");
        OrderValidationResponse response = service.update(orderWithCorrectStateAndTaxRate);
        BigDecimal expectedUnchangedTaxRate = new BigDecimal("0.00");
        assertEquals(expectedUnchangedTaxRate, response.getOrder().getTaxRate());
        BigDecimal expectedUnchangedMaterialCostPerSquareFoot = new BigDecimal("0.10");
        assertEquals(expectedUnchangedMaterialCostPerSquareFoot, response.getOrder().getMaterialCostPerSquareFoot());
        BigDecimal expectedUnchangedLaborCostPerSquareFoot = new BigDecimal("0.25");
        assertEquals(expectedUnchangedLaborCostPerSquareFoot, response.getOrder().getLaborCostPerSquareFoot());
        BigDecimal expectedUnchangedMaterialCost = new BigDecimal("1.00");
        assertEquals(expectedUnchangedMaterialCost, response.getOrder().getMaterialCost());
        BigDecimal expectedUnchangedLaborCost = new BigDecimal("2.50");
        assertEquals(expectedUnchangedLaborCost, response.getOrder().getLaborCost());
        BigDecimal expectedUnchangedTax = new BigDecimal("0.00");
        assertEquals(expectedUnchangedTax, response.getOrder().getTax());
        BigDecimal expectedUnchangedTotal = new BigDecimal("3.50");
        assertEquals(expectedUnchangedTotal, response.getOrder().getTotal());
    }
}